a = 3.0

if (isinstance(a,str)) == True:
    print ("Zmienna jest tekstem")

if (isinstance(a,int)) == True:
    print ("Typ zmiennej: calkowita")
    print ("Wartosc zmiennej: calkowita")

if (isinstance(a,float)) == True:
    print ("Typ zmiennej: calkowita")
    if a.is_integer() == True:
        print ("Wartosc zmiennej: calkowita")
    if a.is_integer() == False:
         print ("Wartosc zmiennej: rzeczywista")

# Zadanie 2.1
import turtle
zolw=turtle.Pen()
zolw.speed(1)
bok = 200
zolw.forward(bok)
zolw.left(90)
zolw.forward(bok)
zolw.left(90)
zolw.forward(bok)
zolw.left(90)
zolw.forward(bok*2)
zolw.right(90)
zolw.forward(bok)
zolw.right(90)
zolw.forward(bok)
zolw.right(90)
zolw.forward(bok)
turtle.done()



#Zadanie 2.2
import math
import turtle
turtle.setworldcoordinates(-400, 0, 400, 800)
zolw=turtle.Pen()
zolw.speed(1)
szerokosc=600
wysokosc=200
szer_drzwi=50
wys_drzwi=90
zolw.forward(szerokosc/2)
zolw.left(90)
zolw.forward(wysokosc)
zolw.left(90)
zolw.forward(szerokosc)
zolw.right(135)
zolw.forward(szerokosc/math.sqrt(2))
zolw.right(90)
zolw.forward(szerokosc/math.sqrt(2))
zolw.right(135)
zolw.forward(szerokosc)
zolw.left(90)
zolw.forward(wysokosc)
zolw.left(90)
zolw.forward((szerokosc/2)-(szer_drzwi/2))
zolw.left(90)
zolw.forward(wys_drzwi)
zolw.right(90)
zolw.forward(szer_drzwi)
zolw.right(90)
zolw.forward(wys_drzwi)
zolw.right(90)
zolw.forward((szerokosc/2)+(szer_drzwi/2))
turtle.done()