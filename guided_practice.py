import numpy as np
import csv
import os

moj_folder="E:/Geography/mgr/1semestr/Programowanie/Repository/programming/guided_practice/"
lista_plikow = os.listdir(moj_folder)

lista=[]
for sciezka in lista_plikow:
    pliki = (os.path.join(moj_folder, sciezka))
    #print(pliki)
    with open (pliki,"r", encoding = "utf8") as f:
        dane=csv.reader(f)
        next(dane,None)
        for row in dane:
            lista.append(row)
            #print(lista)
tablica=np.array(lista)

T = (tablica[:,4].astype(float))
                       
min_temp = min(T)
min_temp_index = np.where(T == min_temp)
min_temp_lokalizacja = tablica[min_temp_index,1]
min_temp_data = tablica [min_temp_index,2]
min_temp_godzina = tablica[min_temp_index,3]  

max_temp = max(T)
max_temp_index = np.where(T == max_temp)
max_temp_lokalizacja = tablica[max_temp_index,1]
max_temp_data = tablica [max_temp_index,2]
max_temp_godzina = tablica[max_temp_index,3]

opady = (tablica[:,-2].astype(float))
stacje = np.where(opady > 0)
nazwy = np.array(tablica[stacje,1]) 
godziny = np.array(tablica[stacje,3]) 


wynik1 = "Minimalna temperatura " + str(min_temp) + " zostala zanotowana na stacji " + str(min_temp_lokalizacja) + " dnia " + str(min_temp_data) + " o godzinie " + str(min_temp_godzina) + "." 
wynik2 = "Maksymalna temperatura " + str(max_temp) + " zostala zanotowana na stacji " + str(max_temp_lokalizacja) + " dnia " + str(max_temp_data) + " o godzinie " + str((max_temp_godzina) + "." 
wynik3 = "Opad wystapil na stacjach " + str(nazwy) + " w godzinach " + str(godziny) + " odpowiednio."
wynik = wynik1 + "\n" + wynik2 + "\n" + wynik3
print(wynik)