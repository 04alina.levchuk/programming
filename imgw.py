import schedule
import numpy as np
import requests
import time
import re

# скачивание данных
url = "https://danepubliczne.imgw.pl/api/data/synop/format/csv"
r = requests.get(url, allow_redirects=True)
dane= r.content
dane = dane.splitlines()

# создание матрици с данными
string = str(dane).replace(" b'", '')
string = str(string).replace("\\xc5\\x82","ł")
string = str(string).replace("\\xc5\\x81","Ł")
string = str(string).replace("\\xc5\\x84","ń")
string = str(string).replace("\\xc5\\x83","Ń")
string = str(string).replace("\\xc5\\x9b","ś")
string = str(string).replace("\\xc5\\x9a","Ś")
string = str(string).replace("\\xc4\\x87","ć")
string = str(string).replace("\\xc4\\x86","ć")
string = str(string).replace("\\xc3\\xb3","ó")
string = str(string).replace("\\xc3\\x93","Ó")
string = str(string).replace("\\xc5\\xbc","ż")
string = str(string).replace("\\xc5\\xbb","Ż")
string = str(string).replace("\\xc4\\x85","ą")
string = str(string).replace("\\xc4\\x84","Ą")
string = str(string).replace("\\xc4\\x99","ę")
string = str(string).replace("\\xc4\\x98","Ę")
string = str(string).replace("\\xc5\\xba","ź")
string = str(string).replace("\\xc5\\xb9","Ź")
rows = string.split(',')
mac = np.array(rows).reshape(-1, 10)
columns = mac[:,[0, 1]]

# показать весь список ИД и назв станций
print("Lista dostępnych stacji:")
print(columns)

# определение номера ИД
wszytskie_id = mac[:,[0]]
while True:
    id_stacji = input("Wpisz numer stacji, dla której chcesz pobrać dane: ")
    if id_stacji not in wszytskie_id: # что сделать, если ИД неправильное
        print("Numer stacji jest nieprawidłowy!")
        id_stacji = input("Podaj prawidłowy numer stacji, lub wpisz stop jeśli chcesz anulować proces: ")
        if id_stacji == "stop":
            print("Operacja została przerwana.")
            exit() # ЗАКРЫТЬ ПРОГРАММУ              
        elif id_stacji in wszytskie_id:  # что сделать, если ИД правильное   
            break
        else:
            continue    
    else:    
        break

# вписывание даты
while True:
    try:
        global data_end
        data_end = input ("Wpisz datę, do której będą gromadzić się dane w formacie 'rok-miesiąc-dzień godzina:minuta:sekunda': ")
        start = time.localtime()        
        end = time.strptime(data_end, "%Y-%m-%d %H:%M:%S")
        roznica = time.mktime(end) - time.mktime(start) # разница между вписаным вмеренем и настоящем
        if roznica < 0:
            data_end = input ("Wpisana data dotyczy czasu przeszłego, kliknij enter by wpisać datę ponownie lub wpisz stop by anulować: ")
            if data_end == "stop":
                print("Operacja została przerwana.")
                exit()
        else:
            print ("Dane będą gromadzić się do:", data_end)
            print ("Rozpoczęto pobieranie danych.") 
            break 
    except: #если что-то пошло не так
        data_end = input ("Zły format daty, kliknij enter by wpisać datę ponownie lub wpisz stop by anulować: ")
        if data_end == "stop":
            print("Operacja została przerwana.")
            exit()

# создание пустых списков
temp_list = [] #список с температурой
data_list = [] #список с датами, куда будуть записываться данные из сайта

# функция - конвертировать текст из сайта на список, чтобы брать оттуда данные
def convert(string): 
    lista_stringow = list(string.split(","))
    return lista_stringow

# дэфф - повторять каждые 5 мин в schedule
def check_5min(): 
    global data_imgw
    url_id = "https://danepubliczne.imgw.pl/api/data/synop/id/" #ссылка ид станции
    r2 = requests.get(url_id + id_stacji, allow_redirects=True)
    dane_id = r2.content #данные конкретной станции
    dane_id = str(dane_id)
    lista = convert(dane_id) # конвертированный список данных

    from operator import itemgetter # извлекание элемента из заданого объекта
    data_imgw = itemgetter(2)(lista) 
    godzina = itemgetter(3)(lista)
    temperatura = itemgetter(4)(lista)
    godzina = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", godzina)
    data_imgw = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", data_imgw)
    temperatura = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", temperatura)
    temperatura = float(temperatura)
    data_imgw = data_imgw + " " + godzina + ":0:0"
    
    if data_imgw not in data_list: #проверяет есть ли новые данные на сайте
        data_list.append(data_imgw) #записывает дату в список с уже существующими датами
        temp_list.append(temperatura) #записывает температуру в список
        print ("Dane zostały pobrane.")
    else:
        print("Nowe dane jeszcze nie zostały wprowadzone. Następne sprawdzenie nastąpi za 5 minut. Proszę czekać.")
        

schedule.every(5).minutes.do(check_5min)
schedule.every().hour.at(":01").do(check_5min) 

while True:
    schedule.run_pending() 
    if time.time() > time.mktime(end):    #когда остановить скачивание
        print ("Pobieranie danych zostało zakończone.")
        break

def sredniaT(lst): 
    return sum(lst) / len(lst)

srednia = sredniaT(temp_list)

data_start = data_list[0]
data_end = data_list[-1]

temp_list.append(srednia)
data_list.append("Srednia temperatura wyniosla: ")
data_list.append("W przedziale czasowym:")
temp_list.append(data_start + " - " + data_end)

file = open("srednia_T.txt", "w") #записать среднюю температуру в файл
for index in range(len(data_list)):
    file.write(str(data_list[index]) + "\t" + str(temp_list[index]) + "\n")
file.close()

print ("Dane statystyczne zostały zapisane do pliku tekstowego 'srednia_T'.")
exit()
